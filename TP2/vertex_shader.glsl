#version 330 core

//------------------------------------------------------------------------
// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 2) in vec3 vtx_normal;
//------------------------------------------------------------------------
layout(location = 1) in vec3 vertexColor;
//------------------------------------------------------------------------
out vec4 o_color;
//------------------------------------------------------------------------
uniform float scale;
uniform vec3 translation;

void main(){

    //Appliquer la translation et la mise à l'échelle
    vec3 pos = vertexPosition_modelspace + translation;
    pos = pos * scale;
    gl_Position =  vec4(pos,1);

    //Assigner la normale à la variable interpolée color
    o_color = vec4(vertexColor.x,vertexColor.y,vertexColor.z,1);//normal;
}

