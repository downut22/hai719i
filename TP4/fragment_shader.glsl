#version 330 core

// Output data
out vec4 FragColor;

in vec4 o_color;
in vec2 o_texcoord;

uniform sampler2D ourTexture;

void main()
{

    FragColor = texture(ourTexture,o_texcoord) * o_color;//vec4(o_texcoord,0.0,0.0);
}
