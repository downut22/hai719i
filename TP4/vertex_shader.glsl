#version 330 core

//------------------------------------------------------------------------
// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vtx_normal;
layout(location = 2) in vec2 uvs;
//------------------------------------------------------------------------
out vec4 o_color;
out vec2 o_texcoord;
//------------------------------------------------------------------------
uniform float scale;
uniform vec3 translation;

void main(){

    vec3 pos = vertexPosition_modelspace + translation;
    pos = pos * scale;
    gl_Position = vec4(pos,1);

    o_color = vec4(vtx_normal.x,vtx_normal.y,vtx_normal.z,1);

    o_texcoord = uvs;
}

