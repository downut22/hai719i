// Local includes
#include "Lights.h"
#include "Shader.h"
#include "Texture.h"
#include "stb_image.h"
// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
// OPENGL includes
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>

Light::~Light() {
	if (m_program != 0) {
		glDeleteProgram(m_program);
	}
}

void Light::init(GLuint m_program_ref){//,std::string skyboxPath) {

	m_program = m_program_ref;//load_shaders("shaders/unlit/vertex.glsl", "shaders/unlit/fragment.glsl");
	check();

	color = {1.0, 1.0, 0.8, 1.0};
	position = {4.0,4.0,4.0};
	intensity = 2;

	//m_skybox = load_shaders("shaders/unlit/vertexSkybox.glsl", "shaders/unlit/fragmentSkybox.glsl");

	//m_skybox_texture = loadSkybox(skyboxPath);

}

/*
unsigned int Light::loadSkybox(std::string path)
{
	unsigned int id;
	glGenTextures(1,&id);
	glBindTexture(GL_TEXTURE_CUBE_MAP,id);

	int width, height, nbChannel;
	for(int i = 0; i < 6; i++)
	{
		std::string path = path + "_" + std::to_string(i);
		unsigned char* data = stbi_load(path.c_str(),&width,&height,&nbChannel,0);
		if(data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,data);
			stbi_image_free(data);
		}
		else{std::cout << "Failed to load cube map texture n° " << i << " at path " << path << std::endl;}
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return id;
}*/

void Light::clear() {
	// nothing to clear
	// TODO: Add the texture or buffer you want to destroy for your material
}

void Light::bind(glm::vec3 camPosition) {
	check();
	glUseProgram(m_program);
	internalBind(camPosition);
}

/*void drawSkybox(glm::vec3 camPosition)
{
	/*glDepthFunc(GL_LEQUAL);

	glBindVertexArray(skyboxVertices);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP,m_skybox_texture);
	glDrawArrays(GL_TRIANGLES,0,36);
	glBindVertexArray(0);
	glDepthFunc(GL_LESS);
}*/

void Light::internalBind(glm::vec3 camPosition) {

	// bind parameters
	GLint colorId = getUniform("light.color");
	glUniform4fv(colorId, 1, glm::value_ptr(color));
	
	GLint posId = getUniform("light.position");
	glUniform3fv(posId, 1, glm::value_ptr(position));

	GLint intId = getUniform("light.intensity");
	glUniform1f(intId, intensity);

	GLint camId = getUniform("cameraPosition");
	glUniform3fv(camId, 1, glm::value_ptr(camPosition));


	// TODO : Add your custom parameters here
}

GLint Light::getAttribute(const std::string& in_attributeName) {
	check();
	return glGetAttribLocation(m_program, in_attributeName.c_str());
}

GLint Light::getUniform(const std::string& in_uniformName) {
	check();
	return glGetUniformLocation(m_program, in_uniformName.c_str());
}
