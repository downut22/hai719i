// Local includes
#include "Material.h"
#include "Shader.h"
#include "Texture.h"
// GLM includes
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
// OPENGL includes
#include <GL/glew.h>
#include <GL/glut.h>

Material::~Material() {
	if (m_program != 0) {
		glDeleteProgram(m_program);
	}
}

void Material::init(
	const std::string& texturePath,
	const std::string& normalPath,
	const std::string& bumpPath,
	const std::string& metalPath,
	const std::string& emissionPath){

	m_program = load_shaders("shaders/unlit/vertex.glsl", "shaders/unlit/fragment.glsl");
	check();

	m_color = {1.0, 1.0, 1.0, 1.0};

	m_texture = texturePath != "" ? loadTexture2DFromFilePath(texturePath) : -1;
	m_normal = normalPath != "" ? loadTexture2DFromFilePath(normalPath) : -1;
	m_bump = bumpPath != "" ? loadTexture2DFromFilePath(bumpPath) : -1;
	m_metal = metalPath != "" ? loadTexture2DFromFilePath(metalPath) : -1;
	m_emission = emissionPath != "" ? loadTexture2DFromFilePath(emissionPath) : -1;

	normalForce = 1;
}

void Material::clear() {
	// nothing to clear
	// TODO: Add the texture or buffer you want to destroy for your material
}

void Material::bind() {
	check();
	glUseProgram(m_program);
	internalBind();
}

void Material::bindTexture(const char* uniformName, int index, GLint texture)
{
	glUniform1i(glGetUniformLocation(m_program,uniformName), index);
	glActiveTexture(GL_TEXTURE0 + index);
	glBindTexture(GL_TEXTURE_2D, texture);
}

void Material::internalBind() {
	// bind parameters
	GLint color = getUniform("color");
	glUniform4fv(color, 1, glm::value_ptr(m_color));

	if (m_texture != -1) {
		bindTexture("colorTexture",0,m_texture);
		glUniform1i(getUniform("useColor"), true);
	}

	if (m_normal != -1) {
		bindTexture("normalTexture",1,m_normal);
		glUniform1i(getUniform("useNormal"), true);
	}

	if (m_bump != -1) {
		bindTexture("bumpTexture",2,m_bump);
		glUniform1i(getUniform("useBump"), true);
	}

	if (m_metal != -1) {
		bindTexture("metalTexture",3,m_metal);
		glUniform1i(getUniform("useMetal"), true);
	}

	if (m_emission != -1) {
		bindTexture("emissionTexture",4,m_emission);
		glUniform1i(getUniform("useEmission"), true);
	}

	GLint nId = getUniform("normalForce");
	glUniform1f(nId, normalForce);

	// TODO : Add your custom parameters here
}

void Material::setMatrices(glm::mat4& projectionMatrix, glm::mat4& viewMatrix, glm::mat4& modelMatrix)
{
	check();
	glUniformMatrix4fv(getUniform("projection"), 1, false, glm::value_ptr(projectionMatrix));
	glUniformMatrix4fv(getUniform("view"), 1, false, glm::value_ptr(viewMatrix));
	glUniformMatrix4fv(getUniform("model"), 1, false, glm::value_ptr(modelMatrix));
}

GLint Material::getAttribute(const std::string& in_attributeName) {
	check();
	return glGetAttribLocation(m_program, in_attributeName.c_str());
}

GLint Material::getUniform(const std::string& in_uniformName) {
	check();
	return glGetUniformLocation(m_program, in_uniformName.c_str());
}
