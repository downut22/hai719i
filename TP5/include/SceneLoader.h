#ifndef SCENE_LOADER_H_DEFINED
#define SCENE_LOADER_H_DEFINED

#include <string>

void loadDataWithAssimp(const std::string& modelPath,
						const std::string& texturePath,
						const std::string& normalPath,
						const std::string& bumpPath,
						const std::string& metalRoughtPath,
						const std::string& emissionPath);
#endif
