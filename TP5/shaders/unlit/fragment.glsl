#version 330 core

in vec3 o_positionWorld;
in vec3 o_normalWorld;
in vec3 o_tangentWorld;
in vec2 o_uv0;
out vec4 FragColor;

uniform vec4 color;

uniform sampler2D colorTexture; uniform bool useColor;
uniform sampler2D normalTexture; uniform bool useNormal;
uniform sampler2D bumpTexture; uniform bool useBump;
uniform sampler2D metalTexture; uniform bool useMetal;
uniform sampler2D emissionTexture; uniform bool useEmission;

uniform samplerCube skybox; uniform bool useSkybox;

struct Light
{
	vec3 position;
	vec4 color;
	float intensity;
};

uniform Light light;
uniform vec3 cameraPosition;
uniform float normalForce;

vec3 getPosition(vec2 usedUV)
{
	if(!useBump){return o_positionWorld;}

	float readPosition = texture(bumpTexture, usedUV).r ;

	vec3 usedPosition = o_positionWorld + (o_normalWorld * ((readPosition*2)-1));

	return usedPosition;
}

vec3 getNormal(vec2 usedUV)
{
	if(!useNormal){return o_normalWorld;}

	vec3 readNormal = texture(normalTexture, usedUV).rgb ;

	vec3 bitangent = cross(o_normalWorld,o_tangentWorld);

	/*vec3 usedNormal = o_normalWorld + (((o_tangentWorld * readNormal.x) + (bitangent * readNormal.y)) * normalForce);*/

	vec3 usedNormal = (o_normalWorld * readNormal.b) + (o_tangentWorld * readNormal.g) + (bitangent * readNormal.r);

	return normalize(usedNormal);
}

float getOcclusion(vec2 usedUV)
{
	if(!useMetal){return 1.0;}
	return texture(metalTexture,usedUV).r;
}

float getMetal(vec2 usedUV)
{
	if(!useMetal){return 0.0;}
	return texture(metalTexture,usedUV).b;
}

float getRought(vec2 usedUV)
{
	if(!useMetal){return -1.0;}
	return texture(metalTexture,usedUV).g;
}

vec4 phong(vec3 lightDirection,vec3 viewDirection,vec3 usedNormal,vec2 usedUV)
{
	float metal = getMetal(usedUV);
	float rought = getRought(usedUV);

	float specCoef = 16;
	if(rought >= 0){specCoef = 16 - (rought * 10);} //Interpoling between 16 and 6 specular power

	float diffuseAngle = dot(lightDirection,usedNormal);

	float specAngle = pow(dot(reflect(lightDirection,usedNormal),viewDirection),specCoef);

	vec4 phongColor = (light.color * diffuseAngle * (1.0 - metal)); //If this is a metal, there is no diffuse
	phongColor += (light.color * specAngle);

	return (phongColor / 2.0) * light.intensity  * getOcclusion(usedUV);
}

void main() {

	vec2 usedUV = vec2(o_uv0.x,1.0-o_uv0.y);   //vec2(o_uv0.x,o_uv0.y);

	vec3 usedPosition = getPosition(usedUV);
	vec3 usedNormal = getNormal(usedUV);


	//-----------------Processing ligth and view direction-----------------


	vec3 lightDirection = light.position - usedPosition;
	lightDirection = normalize(lightDirection);

	vec3 viewDirection = cameraPosition - usedPosition;
	viewDirection = normalize(viewDirection);


	//-----------------Processing phong-----------------


	vec4 phongColor = phong(lightDirection,viewDirection,usedNormal,usedUV);


	//-----------------Adding texture-----------------


    FragColor = useColor ? (texture(colorTexture, usedUV) * color) : color;

    FragColor = vec4(
    	phongColor.r * FragColor.r,
    	phongColor.g * FragColor.g,
    	phongColor.b * FragColor.b,
    	phongColor.a * FragColor.a);

	if(useEmission)
	{
		vec4 emission = texture(emissionTexture,usedUV);

		FragColor += emission;
	}
}
