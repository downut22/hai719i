#version 330 core

in vec3 uv;

uniform samplerCube skybox;

void main() {

	FragColor = texture(skybox,uv);
}
